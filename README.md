Arduino compatible PCB with built-in 8x16 LED screen for Charlieplexing

I am willing to send an assembled board for the top 4 contributors to this project.  You can upload your code to the board using an Arduino programmer to test your code.

This is an on-going project.
The code here will be the Arduino code controlling the LEDs.
I will also include the Kicad design files for anyone who wishes to make their own boards.
